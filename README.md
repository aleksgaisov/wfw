# WFW - wrapper for when

![gif](./pix/gif.gif)

In essence this is a wrapper bash script for [when](http://www.lightandmatter.com/when/when.html), which is a calendar and events management program.
What this script does is it modifies the original output of the program to my personal liking as well as introduces new event types.

## Usage

One obvious prerequisite is having *when* program installed on your system. 

After downloading the bash script: make it executable with `chmod +x wfw` and add it to PATH for convenience.

The script accepts three arguments: **d**, **w** and **m**; which tells it to display tasks for **day**, **week** and **month** respectively.
Thus, the following commands are possible with the script:
* `wfw d` - display events that are due today
* `wfw w` - display weekdays and their corresponding events
* `wfw m` - display current and the following two months and their corresponding events

## Explanations

The instructions for setting up [when](http://www.lightandmatter.com/when/when.html) could be obtained by running `man when`.

The change tackles `~/.when/calendar` file by adding **recurring** and **deadline** event types.
A **regular** event entry in *when* would look something like this: `2022 aug 4 , Test event`.
**Regular** events are ONLY displayed with **d** and **w** flag.

By adding `R:` to the entry; i.e `w=fri , R: Backup every Friday`; would mark it as a **recurring**.
**Recurring** events are ONLY displayed with the **d** flag.

By adding `D:` to the entry; i.e `2022 sep 1 , D: First day of school`; would mark it as a **deadline**.
**Deadline** events are the ONLY events that are displayed with **m** flag.